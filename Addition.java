import java.util.Scanner;
public class Addition(){
    public static void main(String[] args){
        int num1,num2,result;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter first number: ");
        num1 = scan.nextInt();
        System.out.println("Enter second number: ");
        num2 = scan.nextInt();
        //Addition of 2 num and storing in result variable
        result = num1 + num2;
        System.out.println("Result is "+result);
    }
}